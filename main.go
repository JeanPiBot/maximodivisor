package main

import "fmt"


func main() {
	var numero1, numero2 int
	fmt.Println("Escriba el primer número")
	fmt.Scanf("%d", &numero1)
	fmt.Println("Escriba el segundo número")
	fmt.Scanf("%d", &numero2)

	if numero1 > 0 && numero2 > 0 {
		results := MaximoComunDivisor(numero1, numero2)
		fmt.Println("Tiene como máximo común divisor el valor de ", results)
	} else {
		fmt.Println("Los números no pertenencen a los enteros positivos")
	}
}

func MaximoComunDivisor(number1, number2 int) int {
	var maximo int
	for number1 > 0 {
		if number1 < number2 {
			maximo = number1
			number1 = number2
			number2 = maximo
		}

		number1 -= number2 
	}
	return number2
}