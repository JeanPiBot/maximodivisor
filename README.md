#  Cálculo del máximo común divisor desde un programa principal

Diseñar un algoritmo compuesto por un procedimiento principal identificando como  Principal_MCD, el cual lee dos números, verificando que pertenecen a los enteros positivos; llame a una función identificada como MCD, la cual devuelve el máximo común divisor entre los números; e imprima el MCD  en el programa principal.


# Run and Building

If you just want to run, you can use a command which it is:

```
go run main.go
```

Then build it with the go tool:

```
go build main.go
```

The command above will build an executable named main in the current directory alongside your source code. Execute it to see extraction of data:

```
./main.go
```

__NOTE:__ Just is executable for UNIX system. For execute into Windows system click on the main.exe


# Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


# Authors
* **Jean Pierre Giovanni Arenas Ortiz**

# License
[MIT](https://choosealicense.com/licenses/mit/)